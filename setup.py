from glob import glob
from setuptools import setup, find_packages

scripts = glob('bin/*')

setup(name='keras-unet-cerebellum',
      version='0.3.0',
      description='Cerebellum parcellation',
      author='Shuo Han',
      author_email='shan50@jhu.edu',
      packages=find_packages(),
      scripts=scripts)
