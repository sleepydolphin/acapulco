#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Predict bbox')
parser.add_argument('images', nargs='+',
                    help='The path to the image to predict')
parser.add_argument('-m', '--model', required=True,
                    help='The path to the model')
parser.add_argument('-c', '--target-bbox-shape', default=[128, 96, 96],
                    nargs=3, type=int, help='The shape of extended bbox')
parser.add_argument('-o', '--output-dir', required=False, default='.')
args = parser.parse_args()

import os
import numpy as np
import nibabel as nib
from keras.models import load_model
from keras_contrib.layers import InstanceNormalization

from keras_unet_cerebellum import calc_smooth_l1_loss

custom_objects = {'calc_smooth_l1_loss': calc_smooth_l1_loss,
                  'InstanceNormalization': InstanceNormalization}
model = load_model(args.model, custom_objects=custom_objects)

if not os.path.isdir(args.output_dir):
    os.makedirs(args.output_dir)

for image in args.images:

    image_obj = nib.load(image)
    image_data = image_obj.get_data()[None, None, ...]

    bbox = model.predict(image_data)[0, ...]
    int_bbox = np.round(bbox).astype(int)
    mask = np.zeros(image_data.shape[-3:], dtype=float)
    mask[int_bbox[0]:int_bbox[1], int_bbox[2]:int_bbox[3],
         int_bbox[4]:int_bbox[5]] = 1.0

    image_basename = os.path.basename(image)
    mask_basename = image_basename.replace('.nii.gz', '_mask.nii.gz')
    mask_filepath = os.path.join(args.output_dir, mask_basename)
    mask_obj = nib.Nifti1Image(mask, image_obj.affine, image_obj.header)
    mask_obj.to_filename(mask_filepath)

    bbox_filepath = mask_filepath.replace('mask.nii.gz', 'bbox.csv')
    with open(bbox_filepath, 'w') as bbox_file:
        bbox_file.write(','.join(['%.4f'%num for num in bbox]))
    
    dx = args.target_bbox_shape[0] - (int_bbox[1] - int_bbox[0])
    dy = args.target_bbox_shape[1] - (int_bbox[3] - int_bbox[2])
    dz = args.target_bbox_shape[2] - (int_bbox[5] - int_bbox[4])
    extended_bbox = [int_bbox[0] - dx//2, int_bbox[1] + dx//2,
                     int_bbox[2] - dy//2, int_bbox[3] + dy//2,
                     int_bbox[4] - dz//2, int_bbox[5] + dz//2]
    
    extended_bbox_filepath = mask_filepath.replace('mask.nii.gz',
                                                   'extended_bbox.csv')
    with open(extended_bbox_filepath, 'w') as bbox_file:
        bbox_file.write(','.join(['%d'%num for num in extended_bbox]))
