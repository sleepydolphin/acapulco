#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Predict bbox and parcellation')
parser.add_argument('-i', '--images', nargs='+', required=True,
                    help='The path to the image to predict; only support nifti')
parser.add_argument('-b', '--bbox-model', required=False,
                    help='The path to the bounding box model')
parser.add_argument('-p', '--parc-model', required=True,
                    help='The path to the parcellation model')
parser.add_argument('-l', '--labels', required=True,
                    help='A json file containing the label descriptions')
parser.add_argument('-o', '--output-dirs', nargs='+', required=True)
parser.add_argument('-s', '--image-shape', default=[193, 229, 193],
                    type=int, nargs=3, help='The shape of the input image.')
parser.add_argument('-c', '--target-bbox-shape', default=[128, 96, 96],
                    nargs=3, type=int, help='The shape of extended bbox')
parser.add_argument('-ms', '--mask-suffix', default='_mask')
parser.add_argument('-ss', '--seg-suffix', default='_seg')
parser.add_argument('-ps', '--pred-suffix', default='_pred')
parser.add_argument('-t', '--output-type', choices=['.nii.gz', '.nii'],
                    default='.nii.gz', help='The output file nifti type')
parser.add_argument('-u', '--mni-mask',
                    help='The path to a cerebellum mask in MNI space')

args = parser.parse_args()

import os
import json
import numpy as np
import nibabel as nib

import sys
stderr = sys.stderr
sys.stderr = open(os.devnull, 'w')
from keras.models import load_model
sys.stderr = stderr

from keras_contrib.layers import InstanceNormalization
from image_processing_3d import crop3d, uncrop3d, resize_bbox3d
from image_processing_3d import padcrop3d, calc_bbox3d

from keras_unet_cerebellum import calc_smooth_l1_loss, calc_aver_dice_loss


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

if args.mni_mask is None:
    custom_objects = {'calc_smooth_l1_loss': calc_smooth_l1_loss,
                      'InstanceNormalization': InstanceNormalization}
    bbox_model = load_model(args.bbox_model, custom_objects=custom_objects)

custom_objects = {'calc_aver_dice_loss': calc_aver_dice_loss,
                  'InstanceNormalization': InstanceNormalization}
parc_model = load_model(args.parc_model, custom_objects=custom_objects)

with open(args.labels) as jfile:
    labels = sorted(tuple(int(l) for l in json.load(jfile)['labels'].values()))

if len(args.output_dirs) < len(args.images):
    args.output_dirs = [args.output_dirs[0]] * len(args.images)

for image_path, output_dir in zip(args.images, args.output_dirs):
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    image_obj = nib.load(image_path)
    image = image_obj.get_data()
    resized, source_bbox_r, target_bbox_r = padcrop3d(image, args.image_shape)

    if args.mni_mask is None:
        bbox = bbox_model.predict(resized[None, None, ...])[0, ...]
        int_bbox = np.round(bbox).astype(int)
        bbox = [slice(int_bbox[0], int_bbox[1]),
                slice(int_bbox[2], int_bbox[3]),
                slice(int_bbox[4], int_bbox[5])]
    else:
        print('    Use MNI cerebellum mask')
        mask = nib.load(args.mni_mask).get_fdata() > 0.5
        mask = padcrop3d(mask, args.image_shape)[0]
        bbox = calc_bbox3d(mask)
        int_bbox = [bbox[0].start, bbox[0].stop,
                    bbox[1].start, bbox[1].stop,
                    bbox[2].start, bbox[2].stop]

    extended_bbox = resize_bbox3d(bbox, args.target_bbox_shape)

    cropped, source_bbox_c, target_bbox_c = crop3d(resized, extended_bbox)
    pred = parc_model.predict(cropped[None, None, ...])[0, ...]
    if pred.shape[0] == 1: # binary prediction
        seg = np.squeeze(pred) > 0.5
    else:
        seg = np.argmax(pred, axis=0)
        labels = np.sort(labels)
        seg = labels[seg]

    if image_path.endswith('.nii.gz'):
        ext = '.nii.gz'
    elif image_path.endswith('.nii'):
        ext = '.nii'
    else:
        raise RuntimeError('The image path should end with .nii/.nii.gz')

    image_basename = os.path.basename(image_path)
    mask_suffix = args.mask_suffix + args.output_type
    mask_basename = image_basename.replace(ext, mask_suffix)
    mask_filepath = os.path.join(output_dir, mask_basename)

    seg_suffix = args.seg_suffix + args.output_type
    seg_basename = image_basename.replace(ext, seg_suffix)
    seg_filepath = os.path.join(output_dir, seg_basename)

    mask = np.zeros(resized.shape, dtype=float)
    mask[int_bbox[0]:int_bbox[1], int_bbox[2]:int_bbox[3],
         int_bbox[4]:int_bbox[5]] = 1
    mask = uncrop3d(mask, image.shape, source_bbox_r, target_bbox_r)
    mask_obj = nib.Nifti1Image(mask, image_obj.affine, image_obj.header)
    mask_obj.to_filename(mask_filepath)

    seg = uncrop3d(seg, resized.shape, source_bbox_c, target_bbox_c)
    seg = uncrop3d(seg, image.shape, source_bbox_r, target_bbox_r)
    seg_obj = nib.Nifti1Image(seg, image_obj.affine, image_obj.header)
    seg_obj.to_filename(seg_filepath)

    for i, chan in enumerate(pred):
        pred_dirname = image_basename.replace(ext, args.pred_suffix)
        pred_dirname = os.path.join(output_dir, pred_dirname)
        if not os.path.isdir(pred_dirname):
            os.makedirs(pred_dirname)
        chan_basename = '%02d%s' % (i, args.output_type)
        chan_filepath = os.path.join(pred_dirname, chan_basename)
        chan = uncrop3d(chan, resized.shape, source_bbox_c, target_bbox_c)
        chan = uncrop3d(chan, image.shape, source_bbox_r, target_bbox_r)
        chan_obj = nib.Nifti1Image(chan, image_obj.affine, image_obj.header)
        chan_obj.to_filename(chan_filepath)
