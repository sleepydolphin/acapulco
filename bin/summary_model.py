#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
parser = argparse.ArgumentParser(description='Summary keras model')
parser.add_argument('input_model', help='Path to the model to summary')
args = parser.parse_args()

from keras.utils import plot_model
from keras.models import load_model
from keras_contrib.layers import InstanceNormalization

from keras_unet_cerebellum import calc_aver_dice_loss, calc_smooth_l1_loss


co = {'InstanceNormalization': InstanceNormalization,
      'calc_aver_dice_loss': calc_aver_dice_loss,
      'calc_smooth_l1_loss': calc_smooth_l1_loss}
model = load_model(args.input_model, custom_objects=co)
model.summary()
for layer in model.layers:
    if 'model' in layer.name:
        layer.summary()
