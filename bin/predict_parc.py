#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser(description='Predict using trained model')
parser.add_argument('-i', '--images', nargs='+', required=True,
                    help='The path to the image to predict')
parser.add_argument('-a', '--masks', nargs='+', required=True,
                    help='The path to the ROI mask')
parser.add_argument('-c', '--cropping-shape', nargs=3, type=int, required=True,
                    help='The shape of the bounding box to crop ROI')
parser.add_argument('-o', '--output-dir', required=True, 
                    help='The folder to save the prediction; the output will '
                         'be named as ${image_filename}_pred.nii.gz')
parser.add_argument('-m', '--model', required=True,
                    help='The path to the model')
parser.add_argument('-l', '--label-image', required=True,
                    help='The path to a label image for one-hot decoding')
args = parser.parse_args()


import os
import numpy as np
from keras.models import load_model
from keras_contrib.layers import InstanceNormalization
from network_utils.data_decorators import Cropping3d
from network_utils.data import Data3d
from image_processing_3d import uncrop3d

from keras_unet_cerebellum import calc_aver_dice_loss


custom_objects={'calc_aver_dice_loss': calc_aver_dice_loss,
                'InstanceNormalization': InstanceNormalization}
model = load_model(args.model, custom_objects=custom_objects)

if not os.path.isdir(args.output_dir):
    os.makedirs(args.output_dir)


for image_path, mask_path in zip(args.images, args.masks):

    image = Data3d(image_path)
    mask = Data3d(mask_path)
    cropped_image = Cropping3d(image, mask, args.cropping_shape)
    cropped_image_data = cropped_image.get_data()[None, ...]
    pred = model.predict(cropped_image_data)[0, ...]

    labels = np.unique(Data3d(args.label_image).get_data())
    if pred.shape[0] == 1: # binary prediction
        prediction = np.squeeze(pred)
        seg = (pred > 0.5) * labels[1]
    else:
        seg = np.argmax(pred, axis=0)
        seg = labels[seg]
    uncrop_seg = uncrop3d(seg, image.get_data().shape,
                          cropped_image._source_bbox,
                          cropped_image._target_bbox)[0, ...]
    print(uncrop_seg.shape)

    if image_path.endswith('.nii.gz'):
        import nibabel as nib
        image_obj = nib.load(image_path)
        seg_obj = nib.Nifti1Image(uncrop_seg, image_obj.affine,
                                  image_obj.header)
        basename = os.path.basename(image_path)
        seg_basename = basename.replace('.nii.gz', '_seg.nii.gz')
        seg_obj.to_filename(os.path.join(args.output_dir, seg_basename))
